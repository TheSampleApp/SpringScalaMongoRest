package com.the.sample.app.controller

import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import org.springframework.web.bind.annotation.{
  DeleteMapping,
  GetMapping,
  PathVariable,
  PostMapping,
  PutMapping,
  RequestBody,
  RequestMapping,
  RestController
}
import reactor.core.publisher.{ Flux, Mono }

@RestController
@RequestMapping(Array("/users"))
class UserRestController(userService: UserService) {
  @GetMapping(Array("/{id}")) def getUserById(@PathVariable("id") id: String): Mono[User] =
    userService.findById(id)

  @GetMapping(Array("/")) def getAllUsers(): Flux[User] =
    userService.findAll()

  @PostMapping(Array("/")) def saverUser(@RequestBody user: User): Mono[User] = {
    userService.save(user)
    Mono.just(user)
  }

  @PutMapping(Array("/{id}")) def updateUser(@PathVariable("id") id: String, @RequestBody user: User): Mono[User] = {
    user.id = id
    userService.save(user)
    Mono.just(user)
  }

  @DeleteMapping(Array("/{id}")) def deleteUser(@PathVariable("id") id: String): Unit =
    userService.deleteById(id)
}
