package com.the.sample.app.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

import scala.beans.BeanProperty

@Document
class User (@BeanProperty
            var fullName: String,
            @BeanProperty
            var email: String){
  @Id
  var id: String = _
  def this() = this(null,null)
}
